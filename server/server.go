package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"time"

	"storkgo/internal/service"
	"storkgo/pkg/api"

	"google.golang.org/grpc"
)

// var (
// 	ciCommitBranch,
// 	ciCommitTag,
// 	ciCommitShortSha,
// 	ciCommitSha,
// 	gitVersion string
// )

// func init() {
// 	fmt.Printf("Branch:%s\nTag:%s\nShortSha:%s\nSha:%s\nVersion:%s\n",
// 		ciCommitBranch,
// 		ciCommitTag,
// 		ciCommitShortSha,
// 		ciCommitSha,
// 		gitVersion,
// 	)
// }

func main() {

	listen, err := net.Listen("tcp4", fmt.Sprintf("0.0.0.0:%d", 9009))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	grpcServer := grpc.NewServer()
	ps := &service.GRPCServer{}
	api.RegisterMyServiceServer(grpcServer, ps)

	go func() {
		log.Printf("grpc: starting %s\n", listen.Addr().String())
		if err := grpcServer.Serve(listen); err != nil {
			log.Fatalf("failed to grpc serve: %s", err)
		}
	}()

	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)

	// Block until we receive our signal.
	<-c

	// Create a deadline to wait for.
	_, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()
	// Doesn't block if no connections, but will otherwise wait
	// until the timeout deadline.
	grpcServer.Stop()
	// Optionally, you could run srv.Shutdown in a goroutine and block on
	// <-ctx.Done() if your application should wait for other services
	// to finalize based on context cancellation.
	log.Println("shutting down")
	os.Exit(0)
}
