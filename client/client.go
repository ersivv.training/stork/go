package main

import (
	"flag"
	"log"

	"storkgo/internal/client"
)

var (
	server string
	key    string
	port   int
	app    = client.App{}
)

func init() {
	log.Println("Init client")

	flag.StringVar(&server, "server", "127.0.0.1:9009", "server")
	flag.StringVar(&key, "key", "", "required access key")
	flag.IntVar(&port, "port", 8080, "listen http port")
	flag.Parse()
	if key == "" {
		log.Fatalf("flag key is required")
	}
	log.Printf("Config:\n\tserver: %s\n\tkey: %s\n", server, key)

	app.SetConnection(server, key, port)

}

func main() {
	app.Run()
}
