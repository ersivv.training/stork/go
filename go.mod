module storkgo

go 1.16

require (
	github.com/ReneKroon/ttlcache/v2 v2.3.0
	github.com/caarlos0/env v3.5.0+incompatible
	github.com/golang/protobuf v1.5.1
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/jackc/pgx/v4 v4.10.1
	google.golang.org/grpc v1.36.0
	google.golang.org/protobuf v1.26.0
)
