package client

import (
	"context"
	"encoding/json"
	"net/http"
	"storkgo/pkg/api"

	"github.com/golang/protobuf/jsonpb"
)

// HomeHandler .
func (app *App) HomeHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	w.Write([]byte("Service is running"))
}

// GetUsersHandler .
func (app *App) GetUsersHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Cache-Control", "private, max-age=0, no-cache")

	users, err := app.cache.Get("pbUsers")

	if err == nil {
		w.WriteHeader(http.StatusOK)
		type userType struct {
			UserID int
			Login  string
			Email  string
		}
		r := []*userType{}
		for _, u := range users.(*api.GetUsersResponse).Users {
			user := &userType{
				UserID: int(u.UserId),
				Login:  u.Login,
				Email:  u.Email,
			}
			r = append(r, user)
		}
		json.NewEncoder(w).Encode(r)

	} else {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err.Error())
	}
}

// AuthHandler .
func (app *App) AuthHandler(w http.ResponseWriter, r *http.Request) {
	var p struct {
		LoginOrEmail string
		Password     string
	}
	err := json.NewDecoder(r.Body).Decode(&p)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	} else if p.LoginOrEmail == "" || p.Password == "" {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	resp, err := app.client.AuthUser(context.Background(), &api.AuthUserRequest{
		LoginOrEmail: p.LoginOrEmail,
		Password:     p.Password,
	})
	if err == nil {
		w.WriteHeader(http.StatusOK)
		m := jsonpb.Marshaler{
			EmitDefaults: true,
		}
		js, _ := m.MarshalToString(resp)
		w.Write([]byte(js))
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err.Error())
	}
}

// DataHandler .
func (app *App) DataHandler(w http.ResponseWriter, r *http.Request) {
	var token = r.Header.Get("Authorization")
	if token == "" {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}
	resp, err := app.client.GetTestData(context.Background(), &api.GetTestDataRequest{
		Token: token,
	})
	if err == nil {
		if resp.GetAllowed() {
			w.WriteHeader(http.StatusOK)
			m := jsonpb.Marshaler{
				EmitDefaults: true,
			}
			js, _ := m.MarshalToString(resp)
			w.Write([]byte(js))
		} else {
			w.WriteHeader(http.StatusForbidden)
			return
		}
	} else {
		w.WriteHeader(http.StatusInternalServerError)
		json.NewEncoder(w).Encode(err.Error())
	}
}
