package client

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"storkgo/pkg/api"
	"time"

	"github.com/ReneKroon/ttlcache/v2"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"google.golang.org/grpc"
)

type App struct {
	server string
	key    string
	client api.MyServiceClient
	port   int
	cache  *ttlcache.Cache
}

func (app *App) Run() {
	conn, err := grpc.Dial(app.server, grpc.WithInsecure())

	if err != nil {
		log.Fatalf("not connected %s: %s\n", app.server, err.Error())
	}

	app.client = api.NewMyServiceClient(conn)
	resp, err := app.client.Connect(context.Background(), &api.ConnectRequest{
		Key: app.key,
	})
	if err != nil {
		log.Fatalf("exec Connect error %s\n", err.Error())
	}

	app.cache = ttlcache.NewCache()

	if resp.Result == api.ResultType_SUCCESS {

		app.GetUsers()

		r := mux.NewRouter()
		r.HandleFunc("/", app.HomeHandler).Methods("GET")
		r.HandleFunc("/users", app.GetUsersHandler).Methods("GET")
		r.HandleFunc("/auth", app.AuthHandler).Methods("POST")
		r.HandleFunc("/data", app.DataHandler).Methods("GET")

		headersOk := handlers.AllowedHeaders([]string{"Authorization"})
		originsOk := handlers.AllowedOrigins([]string{"*"})
		methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS"})

		srv := &http.Server{
			Addr:         fmt.Sprintf("0.0.0.0:%d", app.port),
			WriteTimeout: time.Second * 15,
			ReadTimeout:  time.Second * 15,
			IdleTimeout:  time.Second * 60,
			Handler:      handlers.CompressHandler(handlers.CORS(headersOk, originsOk, methodsOk)(r)),
		}

		go func() {
			log.Printf("http: starting %s\n", srv.Addr)
			if err := srv.ListenAndServe(); err != nil {
				log.Fatalf("failed to http serve: %s", err)
			}
		}()

	}

	c := make(chan os.Signal, 1)
	// We'll accept graceful shutdowns when quit via SIGINT (Ctrl+C)
	// SIGKILL, SIGQUIT or SIGTERM (Ctrl+/) will not be caught.
	signal.Notify(c, os.Interrupt)

	ticker := time.NewTicker(10 * time.Second)
	for {
		select {
		case <-ticker.C:
			app.GetUsers()
		case <-c:
			log.Println("shutting down")
			os.Exit(0)
		}
	}

}

func (app *App) SetConnection(server string, key string, port int) {
	app.server = server
	app.key = key
	app.port = port
}

func (app *App) GetUsers() {
	// log.Printf("get users")
	if resp, err := app.client.GetUsers(context.Background(), &api.GetUsersRequest{}); err == nil {
		err := app.cache.Set("pbUsers", resp)
		if err != nil {
			log.Printf("error write cache: %s\n", err.Error())
		}
	}
}
