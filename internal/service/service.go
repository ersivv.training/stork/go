package service

import (
	"context"

	"storkgo/internal/auth"
	"storkgo/internal/cache"
	"storkgo/internal/model"
	"storkgo/pkg/api"
)

// GRPCServer .
type GRPCServer struct{}

// Connect .
func (s *GRPCServer) Connect(ctx context.Context, req *api.ConnectRequest) (*api.ConnectResponse, error) {
	resp := &api.ConnectResponse{
		Result: api.ResultType_UNKNOWN,
	}
	var (
		err     error
		isValid bool
	)

	isValid, err = auth.CheckAuth(req.Key)
	if isValid {
		resp.Result = api.ResultType_SUCCESS
	} else {
		resp.Result = api.ResultType_FAIL
	}

	return resp, err
}

func (s *GRPCServer) GetUsers(ctx context.Context, req *api.GetUsersRequest) (*api.GetUsersResponse, error) {
	resp := &api.GetUsersResponse{
		Users: make([]*api.User, 0),
	}
	var (
		err  error
		data []*model.User
	)

	if data, err = model.GetUsers(); err == nil {
		for _, v := range data {
			user := &api.User{
				UserId: uint32(v.UserID),
				Login:  *v.Login,
				Email:  *v.Email,
			}
			resp.Users = append(resp.Users, user)
		}
	}

	return resp, err
}

func (s *GRPCServer) AuthUser(ctx context.Context, req *api.AuthUserRequest) (*api.AuthUserResponse, error) {

	var (
		resp = &api.AuthUserResponse{
			Token: "",
		}
		isAuth bool
		user   model.User
		err    error
	)

	if isAuth, user, err = auth.AuthUser(req.LoginOrEmail, req.Password); isAuth {
		if token, err := auth.RandToken(32); err == nil {
			resp.User = &api.User{
				UserId: uint32(user.UserID),
				Login:  *user.Login,
				Email:  *user.Email,
			}
			cache.SetUserSessionToken(user.UserID, token)
			resp.Token = token
		}
	}

	return resp, err
}

func (s *GRPCServer) GetTestData(ctx context.Context, req *api.GetTestDataRequest) (*api.GetTestDataResponse, error) {
	var (
		resp = &api.GetTestDataResponse{
			Allowed: false,
		}
		err error
	)
	if cache.ChechExistsToken(req.Token) {
		resp.Allowed = true
		resp.Message = lorem()
	}

	return resp, err
}

func lorem() string {
	return "Lorem ipsum ..."
}
