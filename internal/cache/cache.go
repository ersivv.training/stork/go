package cache

import (
	"time"

	"github.com/ReneKroon/ttlcache/v2"
)

var (
	cache *ttlcache.Cache

	// notFound = ttlcache.ErrNotFound
	// isClosed = ttlcache.ErrClosed

	sessionTtl = 10 * time.Minute
)

func init() {
	cache = ttlcache.NewCache()
}

func SetUserSessionToken(userId int, token string) error {
	return cache.SetWithTTL(token, userId, sessionTtl)
}

func ChechExistsToken(token string) bool {
	var res bool
	if _, err := cache.Get(token); err == nil {
		res = true
	}
	return res
}
