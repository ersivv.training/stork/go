package auth

import (
	"encoding/hex"
	"math/rand"
	"storkgo/internal/model"
	"time"

	"crypto/sha256"
)

var (
	salt = "saltstring"

	allowedKeys = map[string]struct{}{
		"key1": {},
		"key3": {},
	}
)

func init() {
	rand.Seed(time.Now().UTC().UnixNano())
}

func CheckAuth(key string) (bool, error) {
	var (
		resp bool
		err  error
	)
	if _, ok := allowedKeys[key]; ok {
		resp = true
	}

	return resp, err
}

func AuthUser(loginOrEmail string, password string) (bool, model.User, error) {
	var (
		isAuth bool
		user   model.User
		hash   string
		err    error
	)
	if hash, user, err = model.GetUserPasswordHash(loginOrEmail); err == nil {
		isAuth = comparePasswordHash(password, hash)

	}
	return isAuth, user, err
}

func comparePasswordHash(password string, hash string) bool {
	// fmt.Printf("SHA256 database is %s\n", hash)
	return getPasswordHash(password) == hash
}

func getPasswordHash(password string) string {
	sha256Bytes := sha256.Sum256([]byte(password + salt))
	sh := hex.EncodeToString(sha256Bytes[:])
	// fmt.Printf("SHA256 String is %s\n", sh)
	return sh
}

// randToken generates a random hex value.
func RandToken(n int) (string, error) {
	bytes := make([]byte, n)
	if _, err := rand.Read(bytes); err != nil {
		return "", err
	}
	return hex.EncodeToString(bytes), nil
}
