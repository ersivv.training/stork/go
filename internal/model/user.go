package model

import (
	"context"
	"log"
	"strings"

	"github.com/jackc/pgx/v4"
)

// User .
type User struct {
	UserID int
	Login  *string
	Email  *string
}

// GetUsers *
func GetUsers() ([]*User, error) {
	users := make([]*User, 0)

	c, err := Connect()
	if err == nil {
		var rows pgx.Rows
		rows, err = c.Query(context.Background(), `select user_id, login, email from users;`)
		if err != nil && err != pgx.ErrNoRows {
			log.Printf("QueryRows failed: %v\n", err)
		} else {
			for rows.Next() {
				user := User{}
				err = rows.Scan(
					&user.UserID,
					&user.Login,
					&user.Email,
				)
				if err != nil {
					log.Printf("scan failed: %v\n", err)
				}
				users = append(users, &user)
			}
		}

	}
	c.Release()

	return users, err
}

func GetUserPasswordHash(loginOrEmail string) (string, User, error) {
	var (
		hash string
		user = User{}
		err  error
	)

	c, err := Connect()
	if err == nil {
		var rows pgx.Rows
		rows, err = c.Query(context.Background(), `SELECT user_id, login, email, passwd
		FROM public.users
		where login = $1 or email = $1;`, loginOrEmail)
		if err != nil && err != pgx.ErrNoRows {
			log.Printf("QueryRows failed: %v\n", err)
		} else {
			for rows.Next() {
				err = rows.Scan(
					&user.UserID,
					&user.Login,
					&user.Email,
					&hash,
				)
				if err != nil {
					log.Printf("scan failed: %v\n", err)
				}
				hash = strings.Replace(hash, "\\x", "", 1)
			}
		}

	}
	c.Release()

	return hash, user, err
}
