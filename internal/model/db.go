package model

import (
	"context"
	"fmt"
	"log"

	"github.com/caarlos0/env"
	"github.com/jackc/pgx/v4/pgxpool"
)

type DbConfigType struct {
	DbHost string `env:"DB_HOST" envDefault:"timescaledb" json:"DB_HOST"`
	DbPort int    `env:"DB_PORT" envDefault:"5432" json:"DB_PORT"`
	DbName string `env:"DB_NAME" envDefault:"postgres" json:"DB_NAME"`
	DbUser string `env:"DB_USER" envDefault:"postgres" json:"DB_USER"`
	DbPass string `env:"DB_PASS" envDefault:"postgres" json:"DB_PASS"`
}

var (
	dbConfig DbConfigType
	dbUrl    string
	pool     *pgxpool.Pool

	err error
)

func init() {
	dbConfig = DbConfigType{}

	if err := env.Parse(&dbConfig); err != nil {
		fmt.Printf("%+v\n", err)
	}

	dbUrl = fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",
		dbConfig.DbUser,
		dbConfig.DbPass,
		dbConfig.DbHost,
		dbConfig.DbPort,
		dbConfig.DbName,
	)

	pool, err = pgxpool.Connect(context.Background(), dbUrl)
	if err != nil {
		log.Printf("Unable to connection to database %s - %v\n", dbUrl, err)
	} else {
		log.Println("database connenced successfully")
		FirstInit()
	}
	// defer PoolRW.Close()

}

// Connect .
func Connect() (*pgxpool.Conn, error) {
	var err error

	conn, err := pool.Acquire(context.Background())
	if err != nil {
		log.Printf("Unable to acquire a database connection: %v\n", err)
	}
	return conn, err
}

// FirstInit .
func FirstInit() error {
	c, err := Connect()

	if err == nil {
		_, err = c.Exec(context.Background(), `
	DO $$
begin
	if not exists (
		select *
		from pg_tables
		where
			schemaname = 'public'
			and tablename = 'users')
	then
	
	CREATE EXTENSION pgcrypto;

	CREATE TABLE public.users (
		user_id int4 not null GENERATED ALWAYS AS identity PRIMARY KEY,
		register_date timestamp(0) NULL DEFAULT now(),
		login varchar not null unique,
		email varchar not null unique,
		passwd varchar not null
	);
	
	
	INSERT INTO public.users
	(login, email, passwd)
	values
	('admin', 'admin@domain', digest('admin'||'saltstring', 'sha256')),
	('user', 'user@domain', digest('user'||'saltstring', 'sha256'));

	end if;
end $$;
	`)
		if err != nil {
			log.Printf("init database failed: %v\n", err)
		}
	}
	c.Release()

	return err

}
