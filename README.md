# Golang service example

[Задание](./docs/task.md)

Реализовано как клиент-серверное приложение.

Сервер хранит данные в Postgresql и предоставляет grpc сервисы для взаимодействия.

Клиент через эти сервисы запрашивает и отправляет данные, локально предоставляет http сервисы.

### Схема базы данных

![db](./docs/db.png)


### Сервер

Собран в docker-образ и запускается в связке с сервером postgersql через [docker-compose](docker-compose.yaml)

```
docker-compose up -d
```

Для тестирования grpc-сервисов можно использовать [evans](https://github.com/ktr0731/evans)

```
evans --host 127.0.0.1 --port 9009 --path .\api --proto user.proto,service.proto
```

### Клиент

Собраны в исполняемые файлы в [artifact](https://gitlab.com/ersivv.training/stork/go/-/jobs/artifacts/master/download?job=Client) gitlab ci.

Или отдельно [клиент](https://gitlab.com/ersivv.training/stork/go/-/jobs/artifacts/master/raw/out/client/client.win64.exe?job=Client) Windows x64 

При запуске в параметрах доступно задать адрес grpc-сервера, ключ авторизации клиента key (разрешенные для примера 'key1', 'key3'), порт локального http-сервера

```
client.win64.exe -h
Usage of client.win64.exe:
  -key string
        required access key
  -port int
        listen http port (default 8080)
  -server string
        server (default "127.0.0.1:9009")
```

API веб-сервиса клиента описано в [swagger спецификации](./docs/swagger.yaml)

Учтеные данные для защищенного метода:

admin:admin (admin@domain:admin)

user:user (user@domain:user)